/*
 *   Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 *   
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *   
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *   
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */
#include "heap-tree.h"

#undef malloc
#undef calloc
#undef realloc
#undef free
#undef strdup
#include <stdlib.h>
#include <assert.h>

#if defined(__STDC_VERSION__) && (__STDC_VERSION__ == 199901L)
#define tree_inline  inline
#else
#define tree_inline
#endif


#ifdef USE_HEAP_NODE_POOL
heap_node* heap_node_allocate( )
{
	size_t i;

	for( i = 0; i < HG_MAX_ALLOCATIONS; i++ )
	{
		if( !_heap_nodes[ i ].is_used )
		{
			_heap_nodes[ i ].is_used = TRUE;
			return &_heap_nodes[ i ];
		}
	}

	return NULL;
}

void heap_node_free( heap_node *node )
{
	assert( node );
	node->is_used = FALSE;
}
#endif

#define BYTE_PTR( ptr )	                       ((byte *) (ptr))
#define pointer_compare( pointer1, pointer2 )  (BYTE_PTR(pointer1) - BYTE_PTR(pointer2))
#define heap_node_compare( p_node1, p_node2 )  (pointer_compare((p_node1)->block.pointer, (p_node2)->block.pointer))

#define heap_node_init( p_node, p_parent, p_left, p_right, color ) \
	(p_node)->parent = p_parent; \
	(p_node)->left   = p_left; \
	(p_node)->right  = p_right; \
	(p_node)->is_red = color; 


// Typical leaf node (always black)
static heap_node HEAP_NIL = { 
	{
		NULL,
		0,
		{ 0, {'\0'}, {'\0'} },
		{ 0, {'\0'}, {'\0'} },
		FALSE,
		0L
	},
	(heap_node *) &HEAP_NIL, 
	(heap_node *) &HEAP_NIL, 
	(heap_node *) &HEAP_NIL, 
	FALSE 
};


static void       heap_tree_left_rotate  ( heap_tree *p_tree, heap_node *x );
static void       heap_tree_right_rotate ( heap_tree *p_tree, heap_node *x );
static void       heap_tree_insert_fixup ( heap_tree *p_tree, heap_node ** t );
static void       heap_tree_delete_fixup ( heap_tree *p_tree, heap_node ** t );

tree_inline heap_node *heap_node_minimum( heap_node *t )
{
	while( t->left != &HEAP_NIL ) { t = t->left; }
	return t;
}

tree_inline heap_node *heap_node_maximum( heap_node *t )
{
	while( t->right != &HEAP_NIL ) { t = t->right; }
	return t;
}

heap_node *heap_node_successor( heap_node *t )
{
	heap_node *y;
	if( t->right != &HEAP_NIL )
	{
		return heap_node_minimum( t->right );
	}

	y = t->parent;
	
	while( y != &HEAP_NIL && t == y->right )
	{
		t = y;
		y = y->parent;
	}

	return y;
}

heap_node *heap_node_predecessor( heap_node *t )
{
	heap_node *y;

	if( t->left != &HEAP_NIL )
	{
		return heap_node_maximum( t->left );
	}

	y = t->parent;
	
	while( y != &HEAP_NIL && t == y->left )
	{
		t = y;
		y = y->parent;
	}

	return y;
}

tree_inline void heap_tree_left_rotate( heap_tree *p_tree, heap_node *x )
{
	heap_node *y;
	assert( x->right != &HEAP_NIL );

	y        = x->right;
	x->right = y->left;

	if( y->left != &HEAP_NIL )
	{
		y->left->parent = x;
	}

	y->parent = x->parent;

	if( x->parent == &HEAP_NIL )
	{
		p_tree->root = y;
	}
	else if( x == x->parent->left )
	{
		x->parent->left = y;
	}
	else
	{
		x->parent->right = y;
	}

	y->left   = x;
	x->parent = y;
}

tree_inline void heap_tree_right_rotate( heap_tree *p_tree, heap_node *x )
{
	heap_node *y;
	assert( x->left != &HEAP_NIL );

	y       = x->left;
	x->left = y->right;

	if( y->right != &HEAP_NIL )
	{
		y->right->parent = x;
	}

	y->parent = x->parent;

	if( x->parent == &HEAP_NIL )
	{
		p_tree->root = y;
	}
	else if( x == x->parent->right ) {
		x->parent->right = y;
	}
	else
	{
		x->parent->left = y;
	}

	y->right  = x;
	x->parent = y;
}

void heap_tree_init( heap_tree *p_tree )
{
	assert( p_tree );
	p_tree->root    = (heap_node *) &HEAP_NIL;
	p_tree->size    = 0;


#ifdef USE_HEAP_NODE_POOL
	memset( &_heap_nodes[ 0 ], 0, sizeof(heap_node) * HG_MAX_ALLOCATIONS );
#endif

}

void heap_tree_destroy( heap_tree *p_tree, boolean do_free )
{
	heap_tree_clear( p_tree, do_free );
}

/* do not set the pointer in the heap_block */
boolean heap_tree_insert( heap_tree *p_tree, const void * pointer, heap_block **p_block /* out */ )
{
	heap_node *y       = (heap_node *) &HEAP_NIL;
	heap_node *x       = p_tree->root;
#ifdef USE_HEAP_NODE_POOL
	heap_node *newNode = heap_node_allocate( );
#else
	heap_node *newNode = (heap_node *) malloc( sizeof(heap_node) );
#endif
	if( !newNode ) return FALSE;

	newNode->block.pointer = (void *) pointer;
	*p_block               = &newNode->block;
	

	// Find where to insert the new node--y points the parent.	
	while( x != &HEAP_NIL )
	{
		y = x;
		if( heap_node_compare( newNode, x ) < 0 )
		{
			x = x->left;
		}
		else
		{
			x = x->right;
		}
	}

	// Insert the new node.
	if( y == &HEAP_NIL )
	{
		p_tree->root = newNode;
	}
	else 
	{
		if( heap_node_compare( newNode, y ) < 0 )
		{
			y->left = newNode;
		}
		else
		{
			y->right = newNode;
		}
	}

	heap_node_init( newNode, y, (heap_node *) &HEAP_NIL, (heap_node *) &HEAP_NIL, TRUE ); 
	heap_tree_insert_fixup( p_tree, &newNode );
	p_tree->size++;
	
	return TRUE;
}

tree_inline void heap_tree_insert_fixup( heap_tree *p_tree, heap_node ** t )
{
	heap_node *y = (heap_node *) &HEAP_NIL;

	while( (*t)->parent->is_red )
	{
		if( (*t)->parent == (*t)->parent->parent->left ) //is parent on left side of grandparent...
		{
			y = (*t)->parent->parent->right; // y is uncle
			//case 1
			if( y->is_red ) //uncle is red, just recolor
			{
				(*t)->parent->is_red         = FALSE;
				y->is_red                    = FALSE;
				(*t)->parent->parent->is_red = TRUE;
				(*t)                         = (*t)->parent->parent;
			}
			else 
			{
				//case 2 - uncle is black and t is on the right
				if( (*t) == (*t)->parent->right ) 
				{
					(*t) = (*t)->parent;
					heap_tree_left_rotate( p_tree, (*t) );
				}
				//case 3 - uncle is black and t is on the left
				(*t)->parent->is_red         = FALSE;
				(*t)->parent->parent->is_red = TRUE;
				heap_tree_right_rotate( p_tree, (*t)->parent->parent );
			}
		}
		else  // parent is on right side of grandparent...
		{
			y = (*t)->parent->parent->left; // y is uncle
			//case 1
			if( y->is_red ) // uncle is red
			{
				(*t)->parent->is_red         = FALSE;
				y->is_red                    = FALSE;
				(*t)->parent->parent->is_red = TRUE;
				(*t)                         = (*t)->parent->parent;
			}
			else
			{
				//case 2 - uncle is black and t is on the left
				if( (*t) == (*t)->parent->left ) 
				{
					(*t) = (*t)->parent;
					heap_tree_right_rotate( p_tree, (*t) );
				}
				//case 3 - uncle is black and t is on the right
				(*t)->parent->is_red         = FALSE;
				(*t)->parent->parent->is_red = TRUE;
				heap_tree_left_rotate( p_tree, (*t)->parent->parent );
			}
		}
	}

	p_tree->root->is_red = FALSE;
}

boolean heap_tree_remove( heap_tree *p_tree, const void *pointer, boolean do_free )
{
	heap_node *t = heap_node_search( p_tree, pointer );
	heap_node *x;
	heap_node *y;
	if( t == NULL ) return FALSE; // item is not even in the tree!

	if( t->left == &HEAP_NIL || t->right == &HEAP_NIL ) 
	{
		// t has less than two children and can be deleted directly.
		y = t;
	}
	else  // t has two children...
	{
		// t has two children and must be switched out with it's 
		// successor. We will free the successor node and t's data.
		y = heap_node_successor( t );
	}

	if( y->left != &HEAP_NIL )
	{
		x = y->left;
	}
	else
	{
		x = y->right;
	}

	x->parent = y->parent;

	if( y->parent == &HEAP_NIL )
	{
		p_tree->root = x;
	}
	else 
	{
		if( y == y->parent->left )
		{
			y->parent->left = x;
		}
		else
		{
			y->parent->right = x;
		}
	}

	if( y != t )
	{
		if( do_free ) free( (void *) t->block.pointer );

		#ifdef USE_HEAP_NODE_POOL
		heap_node_free( y );
		#else
		free( y );
		#endif

		t->block = y->block;
	}
	else
	{
		if( do_free ) free( (void *) y->block.pointer ); 
		
		#ifdef USE_HEAP_NODE_POOL
		heap_node_free( y );
		#else
		free( y );
		#endif
	}

	if( y->is_red == FALSE ) // y is black
	{
		heap_tree_delete_fixup( p_tree, &x );
	}

	p_tree->size--;

	return TRUE;
}

tree_inline void heap_tree_delete_fixup( heap_tree *p_tree, heap_node ** t )
{
	heap_node *w = (heap_node *) &HEAP_NIL;

	while( (*t) != p_tree->root && (*t)->is_red == FALSE )
	{
		if( (*t) == (*t)->parent->left )
		{
			w = (*t)->parent->right;

			if( w->is_red )
			{
				w->is_red            = FALSE;
				(*t)->parent->is_red = TRUE;
				heap_tree_left_rotate( p_tree, (*t)->parent );
				w = (*t)->parent->right;
			}

			if( w->left->is_red == FALSE && w->right->is_red == FALSE )
			{
				w->is_red = TRUE;
				(*t)      = (*t)->parent;
			}
			else {
				if( w->right->is_red == FALSE )
				{
					w->left->is_red = FALSE;
					w->is_red       = TRUE;
					heap_tree_right_rotate( p_tree, w );
					w = (*t)->parent->right;
				}
			
				w->is_red            = (*t)->parent->is_red;
				(*t)->parent->is_red = FALSE;
				w->right->is_red     = FALSE;
				heap_tree_left_rotate( p_tree, (*t)->parent );
				(*t) = p_tree->root;
			}
		}
		else 
		{
			w = (*t)->parent->left;

			if( w->is_red )
			{
				w->is_red            = FALSE;
				(*t)->parent->is_red = TRUE;
				heap_tree_right_rotate( p_tree, (*t)->parent );
				w = (*t)->parent->left;
			}

			if( w->right->is_red == FALSE && w->left->is_red == FALSE )
			{
				w->is_red = TRUE;
				(*t)      = (*t)->parent;
			}
			else 
			{
				if( w->left->is_red == FALSE )
				{
					w->right->is_red = FALSE;
					w->is_red        = TRUE;
					heap_tree_left_rotate( p_tree, w );
					w = (*t)->parent->left;
				}
			
				w->is_red            = (*t)->parent->is_red;
				(*t)->parent->is_red = FALSE;
				w->left->is_red      = FALSE;
				heap_tree_right_rotate( p_tree, (*t)->parent );
				(*t) = p_tree->root;
			}
		}
	}

	(*t)->is_red = FALSE;
}

boolean heap_tree_search( heap_tree *p_tree, const void *pointer )
{
	heap_node *x = p_tree->root;

	while( x != &HEAP_NIL )
	{
		ptrdiff_t diff = pointer_compare( pointer, x->block.pointer );

		if( diff == 0L )
		{
			return TRUE; // found it
		}
		else if( diff < 0L )
		{
			x = x->left;
		}
		else
		{
			x = x->right;
		}
	}

	return FALSE;
}

void heap_tree_clear( heap_tree *p_tree, boolean do_free )
{
	heap_node *x = p_tree->root;
	heap_node *y = (heap_node *) &HEAP_NIL;

	while( p_tree->size > 0 )
	{
		y = x;

		// find minimum...
		while( y->left != &HEAP_NIL )
		{
			y = y->left;
		}

		if( y->right == &HEAP_NIL )
		{
			y->parent->left = (heap_node *) &HEAP_NIL;
			x = y->parent;

			//free...
			if( do_free ) free( (void *) y->block.pointer );

			#ifdef USE_HEAP_NODE_POOL
			heap_node_free( y );
			#else
			free( y );
			#endif
			
			p_tree->size--;
		}
		else 
		{
			x = y->right;
			x->parent = y->parent;

			if( x->parent == &HEAP_NIL )
			{
				p_tree->root = x;
			}
			else
			{
				y->parent->left = x;
			}
			// free...
			if( do_free ) free( (void *) y->block.pointer );

			#ifdef USE_HEAP_NODE_POOL
			heap_node_free( y );
			#else
			free( y );
			#endif
			p_tree->size--;
		}
	}

	//reset the root and current pointers
	p_tree->root = (heap_node *) &HEAP_NIL;
}

/* ------------------------------------- */

heap_node *heap_node_search( heap_tree *p_tree, const void *pointer )
{
	heap_node *x = p_tree->root;
	
	while( x != &HEAP_NIL )
	{
		ptrdiff_t diff = pointer_compare( pointer, x->block.pointer );

		if( diff == 0L )
		{
			return x;
		}
		else if( diff < 0L )
		{
			x = x->left;
		}
		else
		{
			x = x->right;
		}
	}
	
	return NULL;
}

heap_tree_iterator heap_tree_begin( heap_tree *p_tree )
{
    return heap_node_minimum( p_tree->root );
}

heap_tree_iterator heap_tree_end( )
{
	return (heap_node *) &HEAP_NIL;
}
