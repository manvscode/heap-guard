/*
 *   Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 *   
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *   
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *   
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */
#include <cstddef>
#include <new>
#include "dbg-heap.h"
#undef new
#undef delete


__hg_api Context last_context;

/*
void* operator new( std::size_t size, const char *filename, uint line, const char *function ) throw (std::bad_alloc)
{
	void *p = __malloc( size, filename, line, function );

	if( p == NULL ) // did malloc succeed?
	{
		throw std::bad_alloc(); // ANSI/ISO compliant behavior
	}

	return p;
}

void* operator new( std::size_t size, const std::nothrow_t& nothrow_constant, const char *filename, uint line, const char *function ) throw()
{
	void *p = __malloc( size, filename, line, function );
	return p; // could be NULL
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

void* operator new[]( std::size_t size, const char *filename, uint line, const char *function ) throw (std::bad_alloc)
{
	void *p = __malloc( size, filename, line, function );

	if( p == NULL ) // did malloc succeed?
	{
		throw std::bad_alloc(); // ANSI/ISO compliant behavior
	}

	return p;
}

void* operator new[]( std::size_t size, const std::nothrow_t& nothrow_constant, const char *filename, uint line, const char *function ) throw()
{
	void *p = __malloc( size, filename, line, function );
	return p; // could be NULL
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

void operator delete( void *pointer ) throw ()
{
	__free( pointer, last_context.filename(), last_context.line(), last_context.function() );
}

void operator delete( void* pointer, const std::nothrow_t& nothrow_constant ) throw()
{
	__free( pointer, last_context.filename(), last_context.line(), last_context.function() );
}


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

void operator delete[]( void* pointer ) throw ()
{
	__free( pointer, last_context.filename(), last_context.line(), last_context.function() );
}

void operator delete[]( void* pointer, const std::nothrow_t& nothrow_constant ) throw()
{
	__free( pointer, last_context.filename(), last_context.line(), last_context.function() );
}
*/
