/*
 *   Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 *   
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *   
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *   
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */
#include "heap-guard.h"
/* these must be last */
#undef malloc
#undef calloc
#undef realloc
#undef free
#undef strdup
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <errno.h>
#include <assert.h>
#ifdef LIBSIGSEGV
#include <sigsegv.h>
#endif
#include "types.h"
#include "checksums.h"
#include "heap-tree.h"


#define CHECKSUM( ptr, size )    (heap_checksum( ptr, size ))
#ifdef max
#undef max
#endif
#define max( x, y )              ((x) ^ (((x) ^ (y)) & -((x) < (y))))
#ifdef min
#undef min
#endif
#define min( x, y )              ((y) ^ (((x) ^ (y)) & -((x) < (y))))

static void    heap_guard_deinitialize ( void );


const byte fence_buffer[ ] = { 
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,

	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,

	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,

	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
	FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE, FENCE_BYTE,
};

#define SET_UNITIALIZED_MEMORY( ptr, padded_size ) \
	memset( ptr, UNINITIALIZED_BYTE, padded_size );

#define SET_SENTINELS( ptr, padded_size ) \
	{ \
		memset( BYTE_PTR(ptr), FENCE_BYTE, HEAP_SENTINEL_SIZE ); \
		memset( BYTE_PTR(ptr) + (padded_size) - HEAP_SENTINEL_SIZE, FENCE_BYTE, HEAP_SENTINEL_SIZE ); \
	}

#define BUFFER_UNDERRUN_CHECK( p_block ) \
	(memcmp( (p_block)->pointer, fence_buffer, HEAP_SENTINEL_SIZE ) != 0)

#define BUFFER_OVERRUN_CHECK( p_block ) \
	(memcmp( BYTE_PTR((p_block)->pointer) + PADDED_SIZE((p_block)->size) - HEAP_SENTINEL_SIZE, fence_buffer, HEAP_SENTINEL_SIZE ) != 0)

#define DANGLING_POINTER_CHECK( p_block ) \
	( !(p_block)->is_allocated && CHECKSUM( POINTER_TO_DATA((p_block)->pointer), (p_block)->size ) != (p_block)->hash )

#define MEMORY_LEAK_CHECK( p_block ) \
	( (p_block)->is_allocated ) /* not freed */


#ifdef LIBSIGSEGV
int                heap_guard_crash          ( void* fault_address, int serious );
void               heap_guard_stack_overflow ( int emergency, stackoverflow_context_t scp );
#endif

boolean            heap_check         ( heap_tree *p_collection, flags_t f );

static boolean     heap_block_insert  ( void *ptr, size_t size, const char *filename, uint32 line, const char *function );
static heap_block* heap_block_find    ( void *ptr );
static size_t      heap_checksum      ( const byte *data, size_t size );
HeapErrors         heap_block_check   ( heap_block *p_block, flags_t f );
void               heap_guard_error   ( int error, const void *p_info );

volatile struct __heap_guard {
	heap_tree blocks;
	flags_t flags;
	size_t blocks_allocated;
	size_t blocks_freed;
	size_t largest_allocation;
	size_t smallest_allocation;
	uint32 error_count;

	hg_error_handler on_error;
	hg_block_handler on_alloc;
	hg_block_handler on_free;
} _heap_guard;



static boolean heap_guard_initialized = FALSE;

void heap_guard( flags_t f )
{
	if( !heap_guard_initialized )
	{
		int r;
		/* Register the deinitialization callback. */
		atexit( heap_guard_deinitialize ); 

		heap_tree_init( &_heap_guard.blocks );

		_heap_guard.flags               = f;
		_heap_guard.blocks_allocated    = 0L;
		_heap_guard.blocks_freed        = 0L;
		_heap_guard.largest_allocation  = 0L;
		_heap_guard.smallest_allocation = ULONG_MAX;
		_heap_guard.error_count         = 0;

		/* custom handlers */
		_heap_guard.on_error            = NULL;
		_heap_guard.on_alloc            = NULL;
		_heap_guard.on_free             = NULL;


        #ifdef LIBSIGSEGV
		r = sigsegv_install_handler( heap_guard_crash );
		r = stackoverflow_install_handler( heap_guard_stack_overflow, NULL, 0L );
        #endif

 		heap_guard_initialized = TRUE;	
	}
	else
	{
		_heap_guard.flags = f;
	}

	fprintf( stderr, "Heap Guard. \n" );
	fprintf( stderr, "----------------------------------------------------------------\n" );
}

void heap_guard_deinitialize( void )
{
	boolean noErrorsFound = HG_NO_ERRORS;
	assert( heap_guard_initialized );
	
	heap_guard_initialized = FALSE;
	noErrorsFound          = heap_check( &_heap_guard.blocks, _heap_guard.flags );


	fprintf( stdout, "Largest Allocation: %-10ld   Smallest Allocation: %-10ld \n", _heap_guard.largest_allocation, _heap_guard.smallest_allocation );
	fprintf( stdout, "Fence Size: %-10ld           Errors Found: %-d\n", sizeof(fence_buffer), _heap_guard.error_count );


	heap_tree_destroy( &_heap_guard.blocks, IS_ENABLED(HG_DELAYED_FREE, _heap_guard.flags) );
}

void heap_guard_handlers( hg_error_handler on_error, hg_block_handler on_alloc, hg_block_handler on_free )
{
	/* custom handlers */
	_heap_guard.on_error = on_error;
	_heap_guard.on_alloc = on_alloc;
	_heap_guard.on_free  = on_free;
}

size_t heap_guard_blocks_allocated( void )
{
	return _heap_guard.blocks_allocated;
}

size_t heap_guard_blocks_freed( void )
{
	return _heap_guard.blocks_freed;
}

size_t heap_guard_largest_allocation( void )
{
	return _heap_guard.largest_allocation;
}

size_t heap_guard_smallest_allocation( void )
{
	return _heap_guard.smallest_allocation;
}

uint32 heap_guard_errors( void )
{
	return _heap_guard.error_count;
}

int heap_guard_crash( void* fault_address, int serious )
{
	stack_block block;
	block.fault_address = fault_address;
	
	heap_guard_error( HG_ERR_BAD_ADDRESS, &block );
	
	return 1;
}

#ifdef LIBSIGSEGV
void heap_guard_stack_overflow( int emergency, stackoverflow_context_t scp )
{
	stack_block block;

	block.fault_address = NULL; //Unknown;
	block.eip           = scp->Eip;
	block.ebp           = scp->Ebp;
	block.esp           = scp->Esp;

	heap_guard_error( HG_ERR_STACK_OVERFLOW, &block );
}
#endif



boolean heap_block_insert( void *ptr, size_t size, const char *filename, uint32 line, const char *function )
{ 
	heap_block *p_block = NULL;
	boolean result      = FALSE;
	
#ifdef USE_HEAP_NODE_POOL
	if( _heap_guard.blocks_allocated >= HG_MAX_ALLOCATIONS )
	{ 
		/* Fatal Error -- We ran out of heap_node objects. */
		heap_guard_error( HG_ERR_OUT_OF_MEMORY, NULL ); 
		abort( );
	}
#endif

	result = heap_tree_insert( &_heap_guard.blocks, ptr, &p_block /* out */ );

	if( result ) 
	{ 
		assert( p_block );
		p_block->size                = (size_t) (size); 
		p_block->hash                = 0L; 
		p_block->is_allocated        = TRUE;

		set_code_context( &p_block->allocated, filename, line, function );
		set_code_context( &p_block->freed, UNKNOWN, 0, UNKNOWN );
	} 

	return result;
}

heap_block *heap_block_find( void *ptr )
{
	heap_node *p_node = heap_node_search( &_heap_guard.blocks, ptr );

	if( p_node )
	{
		return &p_node->block;
	}

	return NULL;
}

size_t heap_checksum( const byte *data, size_t size )
{
	size_t hash;

	assert( data );

	if( size > 128 )
	{
		hash = adler32( data, size );
	}
	else // adler-32 performs poorly on small buffers
	{
		hash = java_hash( data, size );
	}

	return hash;
}

boolean heap_check( heap_tree *p_collection, flags_t f )
{
	heap_tree_iterator itr;
	HeapErrors errors = HG_NO_ERRORS;


	for( itr = heap_tree_begin( p_collection );
		 itr != heap_tree_end( );
		 itr = heap_tree_next( itr ) )
	{
		errors |= heap_block_check( &itr->block, f );
	}

	return errors == HG_NO_ERRORS;
}

HeapErrors heap_block_check( heap_block *p_block, flags_t f )
{	
	HeapErrors errors = HG_NO_ERRORS;

	assert( p_block );
	assert( p_block->pointer );

	if( IS_ENABLED(HG_ERR_BUFFER_UNDERRUN, f) && BUFFER_UNDERRUN_CHECK( p_block ) )
	{
		/* Buffer underrun. */
		heap_guard_error( HG_ERR_BUFFER_UNDERRUN, p_block );
		errors |= HG_ERR_BUFFER_UNDERRUN;	
	}

	if( IS_ENABLED(HG_ERR_BUFFER_OVERRUN, f) && BUFFER_OVERRUN_CHECK( p_block ) )
	{
		/* Buffer overflow. */
		heap_guard_error( HG_ERR_BUFFER_OVERRUN, p_block );
		errors |= HG_ERR_BUFFER_OVERRUN;	
	}

	if( IS_ENABLED(HG_ERR_DANGLING_POINTER, f) && DANGLING_POINTER_CHECK( p_block ) )
	{
		/* Dangling pointer was manipulated. */
		heap_guard_error( HG_ERR_DANGLING_POINTER, p_block );
		errors |= HG_ERR_DANGLING_POINTER;	
	}

	if( IS_ENABLED(HG_ERR_MEMORY_LEAK, f) && MEMORY_LEAK_CHECK( p_block ) )
	{
		/* Dangling pointer was manipulated. */
		heap_guard_error( HG_ERR_MEMORY_LEAK, p_block );
		errors |= HG_ERR_MEMORY_LEAK;	
	}


	// TODO: Clean this up.  I think there should be a separate flag
	// to exit on first error.
	if( IS_DISABLED(HG_DELAYED_FREE, f) && errors != HG_NO_ERRORS && heap_guard_initialized )
	{
		/* Because we are freeing immediately we are more susceptible
		 * to crashing so we exit immediately.
		 */
		exit( errors );
	}
	
	return errors;	
}


void heap_guard_error( int error, const void *p_info )
{
	_heap_guard.error_count++;

	if( _heap_guard.on_error )
	{
		_heap_guard.on_error( error, p_info );
	}

	switch( error )
	{
		case HG_ERR_BUFFER_UNDERRUN:
		{
			const heap_block *p_block = (const heap_block *) p_info;
			fprintf( stderr, "(%05d) - BUFFER UNDERRUN DETECTED! \n", _heap_guard.error_count );
			fprintf( stderr, "  Allocated from %s() [%s:%u]\n", p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			if( *p_block->freed.filename) fprintf( stderr, "  Freed from %s() [%s:%u]\n", p_block->freed.function, p_block->freed.filename, p_block->freed.line );
			fprintf( stderr, "  Block Size: %ld bytes \n", p_block->size );
			break;
		}
		case HG_ERR_BUFFER_OVERRUN:
		{
			const heap_block *p_block = (const heap_block *) p_info;
			fprintf( stderr, "(%05d) - BUFFER OVERRUN DETECTED! \n", _heap_guard.error_count );
			fprintf( stderr, "  Allocated from %s() [%s:%u]\n", p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			if( *p_block->freed.filename ) fprintf( stderr, "  Freed from %s() [%s:%u]\n", p_block->freed.function, p_block->freed.filename, p_block->freed.line );
			fprintf( stderr, "  Block Size: %ld bytes \n", p_block->size );
			break;
		}
		case HG_ERR_DANGLING_POINTER:
		{
			const heap_block *p_block = (const heap_block *) p_info;
			fprintf( stderr, "(%05d) - FREED BLOCK MUTATED! \n", _heap_guard.error_count );
			fprintf( stderr, "  Allocated from %s() [%s:%u]\n", p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			if( *p_block->freed.filename) fprintf( stderr, "  Freed from %s() [%s:%u]\n", p_block->freed.function, p_block->freed.filename, p_block->freed.line );
			fprintf( stderr, "  Block Size: %ld bytes \n", p_block->size );
			break;
		}
		case HG_ERR_MEMORY_ALREADY_FREED:
		{
			const heap_block *p_block = (const heap_block *) p_info;
			fprintf( stderr, "(%05d) - MEMORY ALREADY FREED! \n", _heap_guard.error_count );
			fprintf( stderr, "  Allocated from %s() [%s:%u]\n", p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			fprintf( stderr, "  Freed from %s() [%s:%u]\n", p_block->freed.function, p_block->freed.filename, p_block->freed.line );
			fprintf( stderr, "  Block Size: %ld bytes \n", p_block->size );
			break;
		}
		case HG_ERR_MEMORY_NOT_ALLOCATED:
		{
			const heap_block *p_block = (const heap_block *) p_info;
			fprintf( stderr, "(%05d) - FREED MEMORY NEVER ALLOCATED! \n", _heap_guard.error_count );
			fprintf( stderr, "  Freed from %s() [%s:%u]\n", p_block->freed.function, p_block->freed.filename, p_block->freed.line );
			fprintf( stderr, "  Block Size: %ld bytes \n", p_block->size );
			break;
		}
		case HG_ERR_MEMORY_LEAK:
		{
			const heap_block *p_block = (const heap_block *) p_info;
			fprintf( stderr, "(%05d) - MEMORY LEAKED! \n", _heap_guard.error_count );
			fprintf( stderr, "  Allocated from %s() [%s:%u]\n", p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			fprintf( stderr, "  Block Size: %ld bytes \n", p_block->size );
			break;
		}
		case HG_ERR_MEMORY_ALLOC_FAILED:
		{
			const heap_block *p_block = (const heap_block *) p_info;
			fprintf( stderr, "(%05d) - MEMORY ALLOCATION FAILED! \n", _heap_guard.error_count );
			fprintf( stderr, "  Allocation attempt from %s() [%s:%u]\n", p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			fprintf( stderr, "  Requested Block Size: %ld bytes \n", p_block->size );
			break;
		}
		case HG_ERR_OUT_OF_MEMORY:
		{
			const heap_block *p_block = (const heap_block *) p_info;
			fprintf( stderr, "(%05d) - OUT OF MEMORY! \n", _heap_guard.error_count );
			fprintf( stderr, "  You've exhausted all of the available heap blocks. Try changing HG_MAX_ALLOCATIONS in heap-guard.h\n" );
			break;
		}
		case HG_ERR_STACK_OVERFLOW:
		{
			const stack_block *p_block = (const stack_block *) p_info;
			fprintf( stderr, "(%05d) - STACK OVERFLOW! \n", _heap_guard.error_count );
			fprintf( stderr, "  A stack overflow occurred around instruction pointer 0x%X \n", p_block->eip );
			break;
		}
		case HG_ERR_BAD_ADDRESS:
		{
			const stack_block *p_block = (const stack_block *) p_info;
			fprintf( stderr, "(%05d) - BAD ADDRESS! \n", _heap_guard.error_count );
			fprintf( stderr, "  The application crashed when dereferencing 0x%P \n", p_block->fault_address );
			break;
		}
		default:
		{
			fprintf( stderr, "(%05d) - UNKNOWN ERROR! \n" );
			assert( FALSE );
		}
	}

	fprintf( stderr, "----------------------------------------------------------------\n" );
}

		 /********************************
		 *        OVERRIDES              *
		 ********************************/

void* __malloc( size_t size, const char *filename, uint32 line, const char *function )
{
	if( heap_guard_initialized )
	{
		size_t padded_size = PADDED_SIZE( size );
		void *p_memory     = malloc( padded_size );

		if( !p_memory )
		{
			heap_block block; /* block only used for displaying error */

			/* Memory allocation failed */
			errno = ENOMEM;
			assert( FALSE ); 

			block.pointer = NULL;
			block.size    = size;
			set_code_context( &block.allocated, filename, line, function );
			set_code_context( &block.freed, UNKNOWN, 0, UNKNOWN );

			heap_guard_error( HG_ERR_MEMORY_ALLOC_FAILED, &block );
			return NULL;
		}

		SET_UNITIALIZED_MEMORY( p_memory, padded_size );
		SET_SENTINELS( p_memory, padded_size );

		heap_block_insert( p_memory, size, filename, line, function );
		_heap_guard.blocks_allocated++;

		_heap_guard.largest_allocation  = max( size, _heap_guard.largest_allocation );
		_heap_guard.smallest_allocation = min( size, _heap_guard.smallest_allocation );

		return POINTER_TO_DATA( p_memory );
	}

	/*  If Heap Guard is not initialized
	 *  then fail.
	 */
	assert( FALSE );
	
	fprintf( stderr, "Error -- You have attempted to allocate memory before initializing Heap Guard.\n" );
	fprintf( stderr, "         %s() was called on %s:%d\n", filename, line, function );
	return NULL;
}

void* __calloc( size_t count, size_t size, const char *filename, uint32 line, const char *function )
{
	if( heap_guard_initialized )
	{
		size_t data_size   = count * size;
		size_t padded_size = PADDED_SIZE( data_size );
		void *p_memory     = malloc( padded_size );

		if( !p_memory )
		{
			heap_block block; /* block only used for displaying error */

			/* Memory allocation failed */
			errno = ENOMEM;
			assert( FALSE ); 

			block.pointer = NULL;
			block.size    = data_size;
			set_code_context( &block.allocated, filename, line, function );
			set_code_context( &block.freed, UNKNOWN, 0, UNKNOWN );

			heap_guard_error( HG_ERR_MEMORY_ALLOC_FAILED, &block );
			return NULL;
		}

		SET_UNITIALIZED_MEMORY( p_memory, padded_size );
		SET_SENTINELS( p_memory, padded_size );

		heap_block_insert( p_memory, data_size, filename, line, function );
		_heap_guard.blocks_allocated++;

		_heap_guard.largest_allocation  = max( size, _heap_guard.largest_allocation );
		_heap_guard.smallest_allocation = min( size, _heap_guard.smallest_allocation );

		return POINTER_TO_DATA( p_memory );
	}

	/*  If Heap Guard is not initialized
	 *  then fail.
	 */
	assert( FALSE );
	fprintf( stderr, "Error -- You have attempted to allocate memory before initializing Heap Guard.\n" );
	fprintf( stderr, "         %s() was called on %s:%d\n", filename, line, function );
	return NULL;
}

void* __realloc( void *pointer, size_t size, const char *filename, uint32 line, const char *function )
{
	void *p_memory;
	heap_block *p_block;
	
	if( !heap_guard_initialized )
	{
		/*  If Heap Guard is not initialized
		 *  then fail.
		 */
		assert( FALSE );
	
		fprintf( stderr, "Error -- You have attempted to allocate memory before initializing Heap Guard.\n" );
		fprintf( stderr, "         %s() was called on %s:%d\n", filename, line, function );
		return NULL;
	}
	
	p_memory = DATA_TO_POINTER( pointer );	
	p_block  = heap_block_find( p_memory );

	if( p_block )
	{
		size_t padded_size = PADDED_SIZE( size );
		void *p_new_memory = realloc( p_memory, padded_size );


		if( !p_new_memory )
		{
			heap_block block; /* block only used for displaying error */

			/* Memory reallocation failed */
			errno = ENOMEM;
			assert( FALSE ); 

			block.pointer = NULL;
			block.size    = size;
			set_code_context( &block.allocated, filename, line, function );
			set_code_context( &block.freed, UNKNOWN, 0, UNKNOWN );

			heap_guard_error( HG_ERR_MEMORY_ALLOC_FAILED, &block );
			return NULL;
		}

		/* Occasionally, realloc() has to allocate a new block and copy the contents
		 * of the old block over.  In such a case, the pointer stored on the red black
		 * tree becomes invalid. We have to remove the old block and insert a new block.
		 */
		if( p_new_memory != p_memory )
		{
			boolean result;
			heap_tree_remove( &_heap_guard.blocks, p_memory, FALSE /* avoid double free */ );

			p_block = NULL;
			result  = heap_tree_insert( &_heap_guard.blocks, p_new_memory, &p_block /* out */ );

			if( result ) 
			{ 
				assert( p_block );
				p_block->size    = (size_t) size; 
				p_block->hash    = 0L; 

				set_code_context( &p_block->allocated, filename, line, function );
				set_code_context( &p_block->freed, UNKNOWN, 0, UNKNOWN );
			} 
		}

		SET_SENTINELS( p_new_memory, padded_size );

		set_code_context( &p_block->allocated, filename, line, function );
		set_code_context( &p_block->freed, UNKNOWN, 0, UNKNOWN );

		_heap_guard.largest_allocation  = max( size, _heap_guard.largest_allocation );
		_heap_guard.smallest_allocation = min( size, _heap_guard.smallest_allocation );

		return POINTER_TO_DATA( p_new_memory );
	}
	else
	{
		/* You are realloc'ing a pointer that was
		 * was not allocated by heap-guard.
		 */
		heap_block block; /* block only used for displaying error */

		block.pointer = p_memory;
		block.size    = 0L;
		set_code_context( &block.allocated, UNKNOWN, 0, UNKNOWN );
		set_code_context( &block.freed, filename, line, function );

		heap_guard_error( HG_ERR_MEMORY_NOT_ALLOCATED, &block );
		return NULL;
	}
}

void __free( void *pointer, const char *filename, uint32 line, const char *function )
{
	void *p_memory      = DATA_TO_POINTER( pointer );	
	heap_block *p_block = heap_block_find( p_memory );

	if( !heap_guard_initialized )
	{
		assert( heap_guard_initialized );
		fprintf( stderr, "Error -- You have attempted to free memory before initializing Heap Guard.\n" );
		fprintf( stderr, "         %s() was called on %s:%d\n", filename, line, function );
		return;
	}

	if( p_block )
	{
		if( p_block->is_allocated )
		{
			/* Store a hash of the data so that we can check for
			 * dangling pointers being mutated.  Then mark the 
			 * block as freed.
			 */
			p_block->hash         = CHECKSUM( pointer, p_block->size );
			p_block->is_allocated = FALSE;

			/* Save the code location where this block was freed from. */
			set_code_context( &p_block->freed, filename, line, function );
			_heap_guard.blocks_freed++;

			if( IS_DISABLED(HG_DELAYED_FREE, _heap_guard.flags) )
			{
				/* Now, let's search for errors */
				if( heap_block_check( p_block, _heap_guard.flags ) == HG_NO_ERRORS )
				{
					memset( p_memory, FREED_BYTE, PADDED_SIZE(p_block->size) );

					/* We remove the heap block and free it's memory */
					heap_tree_remove( &_heap_guard.blocks, p_memory, TRUE ); 
				}
			}
		}
		else
		{
			heap_guard_error( HG_ERR_MEMORY_ALREADY_FREED, p_block );
		}
	}
	else
	{
		/* You are deleting a pointer that was
		 * was not allocated by heap-guard.
		 */
		heap_block block; /* block only used for displaying error */

		block.pointer = p_memory;
		block.size    = 0L;
		set_code_context( &block.allocated, UNKNOWN, 0, UNKNOWN );
		set_code_context( &block.freed, filename, line, function );

		heap_guard_error( HG_ERR_MEMORY_NOT_ALLOCATED, &block );
	}
}

char *__strdup( const char *string, const char *filename, uint32 line, const char *function )
{
	if( heap_guard_initialized )
	{
		size_t size        = strlen( string ) + 1;
		size_t padded_size = PADDED_SIZE( size );
		void *p_memory     = malloc( padded_size );
	
		if( !p_memory )
		{
			heap_block block; /* block only used for displaying error */

			/* Memory allocation failed */
			assert( FALSE ); 
			errno = ENOMEM;

			block.pointer = NULL;
			block.size    = size;
			set_code_context( &block.allocated, filename, line, function );
			set_code_context( &block.freed, UNKNOWN, 0, UNKNOWN );

			heap_guard_error( HG_ERR_MEMORY_ALLOC_FAILED, &block );
			return NULL;
		}


		SET_SENTINELS( p_memory, padded_size );

		heap_block_insert( p_memory, size, filename, line, function );
		_heap_guard.blocks_allocated++;

		_heap_guard.largest_allocation  = max( size, _heap_guard.largest_allocation );
		_heap_guard.smallest_allocation = min( size, _heap_guard.smallest_allocation );

		strncpy( (char *) POINTER_TO_DATA(p_memory), string, size );

		return (char *) POINTER_TO_DATA( p_memory );
	}

	/*  If Heap Guard is not initialized
	 *  then fail.
	 */
	assert( FALSE );
	return NULL;
}

