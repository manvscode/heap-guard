/*
 *   Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 *   
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *   
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *   
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */
#ifndef _HEAP_TREE_H_
#define _HEAP_TREE_H_

#ifdef __cplusplus
extern "C" {
#endif 

#include <string.h>
#include "types.h"
#include "heap-guard.h"


typedef unsigned char heap_node_flag;




typedef struct _heap_node {   
	heap_block block;
	struct _heap_node *parent;
	struct _heap_node *left;
	struct _heap_node *right;
	boolean is_red;
	boolean is_used;
	//heap_node_flag flags;
} heap_node;


#ifdef USE_HEAP_NODE_POOL
heap_node* heap_node_allocate ( );
void       heap_node_free     ( heap_node *node );

heap_node  _heap_nodes[ HG_MAX_ALLOCATIONS ];
#endif


typedef struct _heap_tree {
	heap_node *root;
	unsigned int size;

} heap_tree;

typedef heap_node* heap_tree_iterator;


void       heap_tree_init        ( heap_tree *p_tree );
void       heap_tree_destroy     ( heap_tree *p_tree, boolean do_free );
boolean    heap_tree_insert      ( heap_tree *p_tree, const void *pointer, heap_block **p_block /* out */ );
boolean    heap_tree_remove      ( heap_tree *p_tree, const void *pointer, boolean do_free );
boolean    heap_tree_search      ( heap_tree *p_tree, const void *pointer );
void       heap_tree_clear       ( heap_tree *p_tree, boolean do_free );

heap_node* heap_node_search      ( heap_tree *p_tree, const void *pointer );
heap_node* heap_node_minimum     ( heap_node *t );
heap_node* heap_node_maximum     ( heap_node *t );
heap_node* heap_node_successor   ( heap_node *t );
heap_node* heap_node_predecessor ( heap_node *t );

heap_tree_iterator heap_tree_begin ( heap_tree *p_tree );
heap_tree_iterator heap_tree_end   ( );

#define heap_tree_size( p_tree )        ((p_tree)->size)
#define heap_tree_is_empty( p_tree )    ((p_tree)->size <= 0)
#define heap_tree_root( p_tree )        ((p_tree)->root)
#define heap_tree_next( p_node )        (heap_node_successor(p_node))
#define heap_tree_previous( p_node )    (heap_node_predecessor(p_node))

#ifdef __cplusplus
}
#endif 
#endif /* _HEAP_TREE_H_ */
