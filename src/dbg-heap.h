/*
 *   Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 *   
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *   
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *   
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */
#ifndef _DBG_HEAP_H_
#define _DBG_HEAP_H_

#include "types.h"

#if defined(HG_DLL) && defined(HG_DLL_EXPORTS)
	#define __hg_api  __declspec( dllexport )
#elif defined(HG_DLL) && defined(HG_DLL_IMPORTS)
	#define __hg_api  __declspec( dllimport )
#else
	#define __hg_api 
#endif

#if defined(__cplusplus) || (defined(__STDC_VERSION__) && (__STDC_VERSION__ == 199901L))
#define __hg_inline   inline
#else
#define __hg_inline   /* not inlined */
#endif


#ifdef __cplusplus
extern "C" {
#endif 


__hg_api void* __malloc   ( size_t size, const char *filename, uint32 line, const char *function );
__hg_api void* __calloc   ( size_t count, size_t size, const char *filename, uint32 line, const char *function );
__hg_api void* __realloc  ( void *pointer, size_t size, const char *filename, uint32 line, const char *function );
__hg_api void  __free     ( void *pointer, const char *filename, uint32 line, const char *function );
__hg_api char* __strdup   ( const char *string, const char *filename, uint32 line, const char *function );


#if defined(WIN32)
	#if defined(BORLAND)
	#define __func__			(__FUNC__)
	#else
	#define __func__			(__FUNCTION__)
	#endif
#endif

extern const byte fence_buffer[ ];

#define UNKNOWN                         ("<unknown>")
#define BYTE_PTR( ptr )	                ((byte *) (ptr))
#define HEAP_SENTINEL_SIZE              (sizeof(fence_buffer))
#define POINTER_TO_DATA(ptr)            (BYTE_PTR(ptr) + HEAP_SENTINEL_SIZE)
#define DATA_TO_POINTER(data_pointer)   (BYTE_PTR(data_pointer) - HEAP_SENTINEL_SIZE)
#define PADDED_SIZE( size )             ((size_t)( 2LU * HEAP_SENTINEL_SIZE + (size) ))

#define FENCE_BYTE                      (0xFE)
#define UNINITIALIZED_BYTE              (0xCD)
#define FREED_BYTE                      (0xDD)
#define STACK_BYTE                      (0xCC)


typedef uint32 HeapErrors;

#define IS_ENABLED( flag, flags )   (((flags) & (flag)) != 0)
#define IS_DISABLED( flag, flags )  (((flags) & (flag)) == 0)
#define IS_SET( flag, flags )       (IS_ENABLED(flag, flags))


#undef malloc
#define malloc( size )         (__malloc( (size_t) (size), __FILE__, __LINE__, __func__ ))
#undef calloc
#define calloc( count, size )  (__calloc( (size_t) (count), (size_t) (size), __FILE__, __LINE__, __func__ ))
#undef realloc
#define realloc( ptr, size )   (__realloc( (void *) (ptr), (size_t) (size), __FILE__, __LINE__, __func__ ))
#undef free
#define free( ptr )            (__free( (void *) (ptr), __FILE__, __LINE__, __func__ ))
#undef strdup
#define strdup( ptr )          (__strdup( (char *) (ptr), __FILE__, __LINE__, __func__ ))


#ifdef __cplusplus
} /* __cplusplus */
#endif 


#ifdef __cplusplus && defined(HG_CPP)

// this is a hack because C++ doesn't allow 
// explicit placement delete syntax.
class __hg_api Context {
  public:
	Context( ) : m_filename(NULL), m_line(0), m_function(NULL) {}
	const char *filename( ) { return m_filename; }
	uint32 line( ) { return m_line; }
	const char *function( ) { return m_function; }

	Context& operator() ( const char *filename, uint32 line, const char *function )
	{
		m_filename = filename;
		m_line     = line;
		m_function = function;
		return *this;
	}

  protected:
	const char *m_filename;
	uint32 m_line;
	const char *m_function;
};

#include <cstddef>
#include <new>
#undef new
#undef delete

extern __hg_api  Context last_context;



/*
void*        operator new      ( std::size_t size, const char *filename, uint32 line, const char *function ) throw (std::bad_alloc);
void*        operator new      ( std::size_t size, const std::nothrow_t& nothrow_constant, const char *filename, uint32 line, const char *function ) throw();
void*        operator new[]    ( std::size_t size, const char *filename, uint32 line, const char *function ) throw (std::bad_alloc);
void*        operator new[]    ( std::size_t size, const std::nothrow_t& nothrow_constant, const char *filename, uint32 line, const char *function ) throw();
void         operator delete   ( void *pointer ) throw ();
void         operator delete   ( void *pointer, const std::nothrow_t& nothrow_constant ) throw();
void         operator delete[] ( void *pointer ) throw ();
void         operator delete[] ( void *pointer, const std::nothrow_t& nothrow_constant ) throw();
*/

__hg_inline void* operator new( std::size_t size, const char *filename, uint32 line, const char *function ) throw (std::bad_alloc)
{
	void *p = __malloc( size, filename, line, function );

	if( p == NULL ) // did malloc succeed?
	{
		throw std::bad_alloc(); // ANSI/ISO compliant behavior
	}

	return p;
}

__hg_inline void* operator new( std::size_t size, const std::nothrow_t& nothrow_constant, const char *filename, uint32 line, const char *function ) throw()
{
	void *p = __malloc( size, filename, line, function );
	return p; // could be NULL
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

__hg_inline void* operator new[]( std::size_t size, const char *filename, uint32 line, const char *function ) throw (std::bad_alloc)
{
	void *p = __malloc( size, filename, line, function );

	if( p == NULL ) // did malloc succeed?
	{
		throw std::bad_alloc(); // ANSI/ISO compliant behavior
	}

	return p;
}

__hg_inline void* operator new[]( std::size_t size, const std::nothrow_t& nothrow_constant, const char *filename, uint32 line, const char *function ) throw()
{
	void *p = __malloc( size, filename, line, function );
	return p; // could be NULL
}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

__hg_inline void operator delete( void *pointer ) throw ()
{
	__free( pointer, last_context.filename(), last_context.line(), last_context.function() );
}

#if !defined(BORLAND)
__hg_inline void operator delete( void* pointer, const std::nothrow_t& nothrow_constant ) throw()
{
	__free( pointer, last_context.filename(), last_context.line(), last_context.function() );
}
#endif

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////

__hg_inline void operator delete[]( void* pointer ) throw ()
{
	__free( pointer, last_context.filename(), last_context.line(), last_context.function() );
}

#if !defined(BORLAND)
__hg_inline void operator delete[]( void* pointer, const std::nothrow_t& nothrow_constant ) throw()
{
	__free( pointer, last_context.filename(), last_context.line(), last_context.function() );
}
#endif


#define __new          new(__FILE__, __LINE__, __func__)
#define new            __new
#define __delete       last_context( __FILE__, __LINE__, __func__ ); delete
#define delete         __delete


#endif /* __cplusplus */
#endif /* _DBG_HEAP_H_ */
