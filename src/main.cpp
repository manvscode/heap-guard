/*
 *   Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 *   
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *   
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *   
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */
#include <iostream>
#include <cassert>
#include "heap-guard.h"
using namespace std;

#define TESTING_UI_DLL

namespace test {

	void crash( )
	{
		unsigned char *pointer = (unsigned char *) 0x00000bad;
		*pointer = 'a'; // Booooom!
	}

	int stack_overflow( )
	{
		int result = 1;
		return stack_overflow( ) + 1;
	}

	void buffer_overrun( unsigned int deviator )
	{
		const int MAX = 30;

		if( deviator > 30 )
		{
			deviator = 30 - 1;
		}

		int *p_i = (int *) malloc( sizeof(int) * MAX );
		int ac = 0xAC;
		memset( p_i, ac, sizeof(int) * (MAX + deviator) ); // overflow!
		free( p_i );
	}

	void buffer_underrun( unsigned int deviator )
	{
		const int MAX = 30;

		if( deviator > 30 )
		{
			deviator = 30 - 1;
		}

		int *p_i = (int *) malloc( sizeof(int) * MAX );
		int ac = 0xAC;
		memset( p_i - deviator, ac, sizeof(int) * MAX ); // overflow!
		free( p_i );
	}

	void freed_block_mutation_detection( )
	{
		int *p_i = (int *) malloc( sizeof(int) );
		*p_i = rand( );
		free( p_i );

		// Now we mutate the dangling pointer
		// to pollute the heap.	
		*p_i = rand( ); 
	}

	void memory_leak_detection( )
	{
		size_t size = (rand( ) % 1000) + 1;
		char *p_string = (char *) malloc( sizeof(char) * size );
		const char *test = "This is a block that was allocated on the heap and not freed.";

		strncpy( p_string, test, size );

	}

	void double_delete_detection( )
	{
		char *p_string = (char *) malloc( sizeof(char) * ((rand() % 32) + 1) );
		free( p_string );
		free( p_string );
	}

	void operator_new( )
	{
		float *number = new float;
		*number = 123.456f;
		delete number;

	}

	void operator_new_array( size_t size )
	{
		int *numbers = new int[ size + 1 ];
	
		for( size_t i = 0; i < size + 1; i++ )
		{
			numbers[ i ] = static_cast<unsigned int>( 2 * i + 1 );
		}

		delete [] numbers;
	}


	void malloc_func( size_t size )
	{
		char *text = (char *) malloc( size + 1 );
		assert( text );
		free( text );
	}

	void calloc_func( size_t count )
	{
		size_t size = sizeof(int);
		short _case = rand() % 3;

		switch( _case )
		{
			case 0:
				size = sizeof(int);
				break;
			case 1:
				size = sizeof(long);
				break;
			case 2:
				size = sizeof(long long);
				break;
			
		}

		void *p_numbers = calloc( count, size );
		assert( p_numbers );

		for( size_t i = 0; i < count; i++ )
		{
			switch( _case )
			{
				case 0:
				{
					int *p_integers = (int *) p_numbers;
					p_integers[ i ] = 2 * i;
					break;
				}
				case 1:
				{
					long *p_integers = (long *) p_numbers;
					p_integers[ i ] = 2L * i;
					break;
				}
				case 2:
				{
					long long *p_integers = (long long *) p_numbers;
					p_integers[ i ] = 200000L * i;
					break;
				}
				
			}
		}

		free( p_numbers );
	}

	void realloc_func( size_t size )
	{
		char *text = (char *) malloc( sizeof(char) * 30 );
		assert( text );
		strcpy( text, "This is a string." );
		char *new_text = (char *) realloc( text, sizeof(char) * (62 + size) );
		assert( new_text );
		strcpy( new_text, "This is a new string. It is even longer now and seems to work." );
		free( new_text );
	}

	void strdup_func( )
	{
		const char *text = "Call me Ishmael. Some years ago- never mind how long precisely- having little or no money in my purse, and nothing particular to interest me on shore, I thought I would sail about a little and see the watery part of the world. It is a way I have of driving off the spleen and regulating the circulation.";
		char *duplicated_string = strdup( text );
		free( duplicated_string );
	}

	class Functor {
	  public:
		void operator()( const char *func )
		{
			printf( "%s() called %s()\n", func, __func__ );
		}
	};

} // test

int main( int argc, char * const argv[] ) 
{
#if 1
	heap_guard( HG_ALL | HG_DELAYED_FREE );
#else
	heap_guard_ui( HG_ALL );
#endif

	
	for( int i = 0; i < 1; i++ )
	{
		/*test::operator_new( );
		test::operator_new_array( rand() % 100 );
		test::malloc_func( (rand( ) % 40) * sizeof(int) );
		test::calloc_func( rand( ) % 100 );
		*/
		test::realloc_func( rand( ) % 5000 );
		test::strdup_func( );
	}



	test::buffer_overrun( rand() % 24 );
	test::buffer_underrun( rand() % 24 );
	test::double_delete_detection( );
	test::memory_leak_detection( );
	test::freed_block_mutation_detection( );
	//test::crash( );
	
	test::stack_overflow( );
	
	//for( int i = 0; i < 30; i++ )
	//{
	//	if( (rand() % 2) == 0 ) test::buffer_overrun( rand() % 24 );
	//	if( (rand() % 3) == 0 ) test::buffer_underrun( rand() % 24 );
	//	if( (rand() % 5) == 0 ) test::double_delete_detection( );
	//	if( (rand() % 7) == 0 ) test::memory_leak_detection( );
	//	if( (rand() % 11) == 0 ) test::freed_block_mutation_detection( );
	//}
	

	return 0;
}
