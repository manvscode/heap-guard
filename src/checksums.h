/*
 *   Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 *   
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *   
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *   
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */
#ifndef _CHECKSUMS_H_
#define _CHECKSUMS_H_

#include <stddef.h>
#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif 

#ifndef MOD_ADLER
#define MOD_ADLER 65521
#endif

uint32 java_hash         ( const byte *data, size_t len ); 
uint32 xor8              ( const byte *data, size_t len );
uint32 adler32           ( const byte *data, size_t len );
uint16 fletcher16_simple ( uint8 *data, size_t len );
void   fletcher16        ( uint8 *checkA, uint8 *checkB, uint8 *data, size_t len ); /* faster */
uint32 fletcher32        ( uint16 *data, size_t len );

#ifdef __cplusplus
} /* __cplusplus */
#endif 
#endif /* _CHECKSUMS_H_ */
