/*
 *   Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 *   
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *   
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *   
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */
#ifndef _HEAP_GUARD_H_
#define _HEAP_GUARD_H_



#include <stddef.h>
#include "dbg-heap.h"

#ifdef __cplusplus
extern "C" {
#endif 

typedef uint16 flags_t;


#define MAX_FILENAME_LENGTH     (255)

typedef struct _code_context {
	unsigned int line;
	char filename[ MAX_FILENAME_LENGTH ];
	char function[ MAX_FILENAME_LENGTH ];
} code_context;

#define set_code_context( p_context, _filename, _line, _function ) \
	assert( p_context ); \
	(p_context)->line = (_line); \
	assert( _filename && *_filename ); \
	strncpy( (p_context)->filename, _filename, sizeof((p_context)->filename) ); \
	(p_context)->filename[ MAX_FILENAME_LENGTH ] = '\0'; \
	assert( _function && *_function ); \
	strncpy( (p_context)->function, _function, sizeof((p_context)->function) ); \
	(p_context)->function[ MAX_FILENAME_LENGTH ] = '\0'; 


typedef struct _heap_block {
	void * pointer;
	size_t size; /* original size.  actual_size = size + padding */
	code_context allocated;
	code_context freed;
	boolean is_allocated; /* is memory still allocated */
	size_t hash;
} heap_block;

typedef struct _stack_block {
	void *fault_address;
	void *eip;
	void *ebp;
	void *esp;
} stack_block;


typedef void (*hg_block_handler)   ( const heap_block *p_block );
typedef void (*hg_error_handler)   ( int error, const void *p_block );

#define HG_ERR_BUFFER_UNDERRUN        (0x0001)
#define HG_ERR_BUFFER_OVERRUN         (0x0002)
#define HG_ERR_DANGLING_POINTER       (0x0004)
#define HG_ERR_MEMORY_ALREADY_FREED   (0x0008)
#define HG_ERR_MEMORY_NOT_ALLOCATED   (0x0010)
#define HG_ERR_MEMORY_LEAK            (0x0020)
#define HG_ERR_MEMORY_ALLOC_FAILED    (0x0040)
#define HG_ERR_OUT_OF_MEMORY          (0x0080)
#define HG_ERR_STACK_OVERFLOW         (0x0100)
#define HG_ERR_BAD_ADDRESS            (0x0200)
#define HG_AUXILIARY_01               (0x0400)
#define HG_AUXILIARY_02               (0x0800)
#define HG_AUXILIARY_03               (0x1000)
#define HG_AUXILIARY_04               (0x2000)
#define HG_AUXILIARY_05               (0x4000)
#define HG_DELAYED_FREE               (0x8000)

#define HG_NONE                       (0x0000)
#define HG_ALL                        (HG_ERR_BUFFER_UNDERRUN | HG_ERR_BUFFER_OVERRUN | \
                                       HG_ERR_DANGLING_POINTER | HG_ERR_MEMORY_ALREADY_FREED | \
                                       HG_ERR_MEMORY_NOT_ALLOCATED | HG_ERR_MEMORY_LEAK | \
                                       HG_ERR_MEMORY_ALLOC_FAILED)
#define HG_NO_ERRORS                  (HG_NONE)


#define USE_HEAP_NODE_POOL 
#ifndef HG_MAX_ALLOCATIONS
#define HG_MAX_ALLOCATIONS		      (4096)
#endif


__hg_api void   heap_guard                     ( flags_t f );
__hg_api void   heap_guard_handlers            ( hg_error_handler on_error, hg_block_handler on_alloc, hg_block_handler on_free );
__hg_api size_t heap_guard_blocks_allocated    ( void );
__hg_api size_t heap_guard_blocks_freed        ( void );
__hg_api size_t heap_guard_largest_allocation  ( void );
__hg_api size_t heap_guard_smallest_allocation ( void );
__hg_api uint32 heap_guard_errors              ( void );

__hg_api void   heap_guard_ui                  ( flags_t f );






#ifdef __cplusplus
 }
#endif 
#endif /* _HEAP_GUARD_H_ */
