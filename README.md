Heap-Guard
====================

Heap Guard (HG) is a simple yet powerful, configurable heap error debugging tool. It can be used with software projects aimed at Linux, Mac OS X, and Windows.

Currently, Heap Guard can detect the following errors:

* Buffer under-runs.
* Buffer overruns.
* Dangling pointer usage.
* Double frees and deletes.
* Memory leaks.
* Allocation failures.
* Out of memory.
* Stack overflows (on Linux only).
* Bad memory address usage (on Linux only).


