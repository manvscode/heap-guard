/*
 *   Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 *   
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *   
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *   
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */
#include "stdafx.h"
#include "heap-guard-ui.h"
#include "errordlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif

//
//TODO: If this DLL is dynamically linked against the MFC DLLs,
//		any functions exported from this DLL which call into
//		MFC must have the AFX_MANAGE_STATE macro added at the
//		very beginning of the function.
//
//		For example:
//
//		extern "C" BOOL PASCAL EXPORT ExportedFunction()
//		{
//			AFX_MANAGE_STATE(AfxGetStaticModuleState());
//			// normal function body here
//		}
//
//		It is very important that this macro appear in each
//		function, prior to any calls into MFC.  This means that
//		it must appear as the first statement within the 
//		function, even before any object variable declarations
//		as their constructors may generate calls into the MFC
//		DLL.
//
//		Please see MFC Technical Notes 33 and 58 for additional
//		details.
//

// HeapGuardUIApp

BEGIN_MESSAGE_MAP(HeapGuardUIApp, CWinApp)
END_MESSAGE_MAP()


// HeapGuardUIApp construction

HeapGuardUIApp::HeapGuardUIApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}


// The one and only HeapGuardUIApp object

HeapGuardUIApp theApp;


void HeapGuardUIApp::onError( int error, const void *p_info )
{
	AFX_MANAGE_STATE( AfxGetStaticModuleState( ) );
	uint error_count = heap_guard_errors( );
	ErrorDlg dlg;
	CString title;
	CString message;


	switch( error )
	{
		case HG_ERR_BUFFER_UNDERRUN:
		{
			const heap_block *p_block = (const heap_block *) p_info;
			title.Format( _T("(%05d)  Buffer Underrun Detected!"), error_count );
			
			message.AppendFormat( _T("    Block Size: %ld bytes\r\n"), p_block->size );
			message.AppendFormat( _T("Allocated From: %s() [%s:%u]\r\n"), p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			if( *p_block->freed.filename ) message.AppendFormat( _T("    Freed From: %s() [%s:%u]\r\n"), p_block->freed.function, p_block->freed.filename, p_block->freed.line );
			
			dlg.setTitle( title );
			dlg.setMessage( message );
			INT_PTR nRet = dlg.DoModal( );
			ASSERT( nRet != IDABORT && nRet != -1 );
			break;
		}
		case HG_ERR_BUFFER_OVERRUN:
		{			
			const heap_block *p_block = (const heap_block *) p_info;
			title.Format( _T("(%05d)  Buffer Overrun Detected!"), error_count );
		
			message.AppendFormat( _T("    Block Size: %ld bytes\r\n"), p_block->size );
			message.AppendFormat( _T("Allocated From: %s() [%s:%u]\r\n"), p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			if( *p_block->freed.filename) message.AppendFormat( _T("    Freed From: %s() [%s:%u]\r\n"), p_block->freed.function, p_block->freed.filename, p_block->freed.line );
			
			dlg.setTitle( title );
			dlg.setMessage( message );

			INT_PTR nRet = dlg.DoModal( );
			ASSERT( nRet != IDABORT && nRet != -1 );
			break;
		}
		case HG_ERR_DANGLING_POINTER:
		{
			const heap_block *p_block = (const heap_block *) p_info;
			title.Format( _T("(%05d)  Freed Block Mutated!"), error_count );

			message.AppendFormat( _T("    Block Size: %ld bytes\r\n"), p_block->size );
			message.AppendFormat( _T("Allocated From: %s() [%s:%u]\r\n"), p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			if( *p_block->freed.filename) message.AppendFormat( _T("    Freed From: %s() [%s:%u]\r\n"), p_block->freed.function, p_block->freed.filename, p_block->freed.line );
			
			dlg.setTitle( title );
			dlg.setMessage( message );
			INT_PTR nRet = dlg.DoModal( );
			ASSERT( nRet != IDABORT && nRet != -1 );
			break;
		}
		case HG_ERR_MEMORY_ALREADY_FREED:
		{
			const heap_block *p_block = (const heap_block *) p_info;
			title.Format( _T("(%05d)  Memory Already Freed!"), error_count );
			
			message.AppendFormat( _T("    Block Size: %ld bytes\r\n"), p_block->size );
			message.AppendFormat( _T("Allocated From: %s() [%s:%u]\r\n"), p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			message.AppendFormat( _T("    Freed From: %s() [%s:%u]\r\n"), p_block->freed.function, p_block->freed.filename, p_block->freed.line );
			
			dlg.setTitle( title );
			dlg.setMessage( message );
			INT_PTR nRet = dlg.DoModal( );
			ASSERT( nRet != IDABORT && nRet != -1 );
			break;
		}
		case HG_ERR_MEMORY_NOT_ALLOCATED:
		{			
			const heap_block *p_block = (const heap_block *) p_info;
			title.Format( _T("(%05d)  Memory Never Alocated!"), error_count );

			message.AppendFormat( _T("Freed from %s() [%s:%u]\r\n"), p_block->freed.function, p_block->freed.filename, p_block->freed.line );
			message.AppendFormat( _T("Block Size: %ld bytes\r\n"), p_block->size );
			
			dlg.setTitle( title );
			dlg.setMessage( message );
			INT_PTR nRet = dlg.DoModal( );
			ASSERT( nRet != IDABORT && nRet != -1 );
			break;
		}
		case HG_ERR_MEMORY_LEAK:
		{		
			const heap_block *p_block = (const heap_block *) p_info;
			title.Format( _T("(%05d)  Memory Leaked!"), error_count );

			message.Format( _T("Allocated from %s() [%s:%u]\r\n"), p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			message.AppendFormat( _T("Block Size: %ld bytes\r\n"), p_block->size );
			
			dlg.setTitle( title );
			dlg.setMessage( message );
			INT_PTR nRet = dlg.DoModal( );
			ASSERT( nRet != IDABORT && nRet != -1 );
			break;
		}
		case HG_ERR_MEMORY_ALLOC_FAILED:
		{		
			const heap_block *p_block = (const heap_block *) p_info;	
			title.Format( _T("(%05d)  Memory Allocation Failed!"), error_count );

			message.Format( _T("Allocation attempt from %s() [%s:%u]\r\n"), p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			message.AppendFormat( _T("Freed from %s() [%s:%u]\r\n"), p_block->freed.function, p_block->freed.filename, p_block->freed.line );
			message.AppendFormat( _T("Requested Block Size: %ld bytes\r\n"), p_block->size );
			
			message.AppendFormat( _T("  Requested Block Size: %ld bytes\r\n"), p_block->size );
			message.AppendFormat( _T("Allocated Attempt From: %s() [%s:%u]\r\n"), p_block->allocated.function, p_block->allocated.filename, p_block->allocated.line );
			message.AppendFormat( _T("            Freed From: %s() [%s:%u]\r\n"), p_block->freed.function, p_block->freed.filename, p_block->freed.line );

			dlg.setTitle( title );
			dlg.setMessage( message );
			INT_PTR nRet = dlg.DoModal( );
			ASSERT( nRet != IDABORT && nRet != -1 );
			break;
		}
		case HG_ERR_OUT_OF_MEMORY:
		{
			title.Format( _T("(%05d)  Out of Memory!"), error_count );

			message.Format( _T("You've used all of the blocks in the heap pool. Try changing the HG_MAX_ALLOCATIONS preprocessor define in heap-guard.h") );

			dlg.setTitle( title );
			dlg.setMessage( message );
			INT_PTR nRet = dlg.DoModal( );
			ASSERT( nRet != IDABORT && nRet != -1 );
			break;
		}
		case HG_ERR_STACK_OVERFLOW:
		{
			const stack_block *p_block = (const stack_block *) p_info;
			title.Format( _T("(%05d)  Stack Overflow!"), error_count );

			message.Format( _T("EIP = 0x%X \n"), p_block->eip );
			message.AppendFormat( _T("EBP = 0x%X \n"), p_block->ebp );
			message.AppendFormat( _T("ESP = 0x%X \n"), p_block->esp );

			dlg.setTitle( title );
			dlg.setMessage( message );
			INT_PTR nRet = dlg.DoModal( );
			ASSERT( nRet != IDABORT && nRet != -1 );

			break;
		}
		case HG_ERR_BAD_ADDRESS:
		{
			const stack_block *p_block = (const stack_block *) p_info;
			title.Format( _T("(%05d)  Bad Address!"), error_count );

			message.Format( _T("The application crashed when dereferencing 0x%P \n"), p_block->fault_address );
			message.AppendFormat( _T("EBP = 0x%X \n"), p_block->ebp );
			message.AppendFormat( _T("ESP = 0x%X \n"), p_block->esp );

			dlg.setTitle( title );
			dlg.setMessage( message );
			INT_PTR nRet = dlg.DoModal( );
			ASSERT( nRet != IDABORT && nRet != -1 );
			break;
		}
		default:
		{
			title.Format( _T("(%05d)  Unknown Error!") );
			message.Format( _T("Please check that you have the latest version.\r\n") );
			
			dlg.setTitle( title );
			dlg.setMessage( message );
			INT_PTR nRet = dlg.DoModal( );
			ASSERT( nRet != IDABORT && nRet != -1 );
		}
	}
}


// HeapGuardUIApp initialization

BOOL HeapGuardUIApp::InitInstance()
{
	CWinApp::InitInstance();

	INITCOMMONCONTROLSEX ccex;

	ccex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	ccex.dwICC  = ICC_STANDARD_CLASSES;

	BOOL bResult = InitCommonControlsEx( &ccex );


	Enable3dControls( );
	EnableModeless( TRUE );


	return bResult;
}


void _onError( int error, const void *pInfo )
{
	theApp.onError( error, pInfo );
}


void heap_guard_ui( flags_t f )
{
	AFX_MANAGE_STATE(AfxGetStaticModuleState());

	heap_guard( f );
	heap_guard_handlers( _onError, NULL, NULL );
}
