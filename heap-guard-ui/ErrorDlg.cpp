/*
 *   Copyright (C) 2010 by Joseph A. Marrero and Shrewd LLC.
 *   
 *   Permission is hereby granted, free of charge, to any person obtaining a copy
 *   of this software and associated documentation files (the "Software"), to deal
 *   in the Software without restriction, including without limitation the rights
 *   to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *   copies of the Software, and to permit persons to whom the Software is
 *   furnished to do so, subject to the following conditions:
 *   
 *   The above copyright notice and this permission notice shall be included in
 *   all copies or substantial portions of the Software.
 *   
 *   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *   THE SOFTWARE.
 */

#include "stdafx.h"
#include "heap-guard-ui.h"
#include "ErrorDlg.h"


// ErrorDlg dialog

IMPLEMENT_DYNAMIC(ErrorDlg, CDialog)

ErrorDlg::ErrorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ErrorDlg::IDD, pParent)
	, m_title(_T("A Heap Error Occurred!"))
	, m_message(_T(""))
{

}

ErrorDlg::~ErrorDlg()
{
}

void ErrorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_ERR_TITLE, m_title);
	DDX_Text(pDX, IDC_ERR_MESSAGE, m_message);
}


BEGIN_MESSAGE_MAP(ErrorDlg, CDialog)
	ON_BN_CLICKED(IDEXIT, &ErrorDlg::OnBnClickedExit)
	ON_WM_CTLCOLOR(WM_CTLCOLOR, &ErrorDlg::OnCtlColor)
END_MESSAGE_MAP()





void ErrorDlg::setTitle( const CString &s )
{
	m_title = s;
}

CString &ErrorDlg::title( )
{ 
	return m_title; 
}

void ErrorDlg::setMessage( const CString &s )
{
	m_message = s;
}

CString &ErrorDlg::message( )
{ 
	return m_message; 
}


BOOL ErrorDlg::OnInitDialog( )
{
	CDialog::OnInitDialog( );

	CStatic *pTitle = reinterpret_cast<CStatic *>( GetDlgItem( IDC_ERR_TITLE) );

	if( pTitle )
	{
		HBITMAP hBitmap = LoadBitmap( AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_MEMORY_CHIP_BMP) );

		CFont titleFont;
		VERIFY(titleFont.CreateFont( 16, 0, 0, 0, FW_BOLD, FALSE, TRUE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DECORATIVE, "Arial" ));
		pTitle->SetFont( &titleFont );
		//pTitle->SetBitmap( hBitmap );
		titleFont.DeleteObject( );

	}
	
	CStatic *pMessage = reinterpret_cast<CStatic *>( GetDlgItem( IDC_ERR_MESSAGE) );

	if( pMessage )
	{
		CFont messageFont;
		VERIFY(messageFont.CreateFont( 10, 0, 0, 0, FW_NORMAL, FALSE, TRUE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_MODERN, _T("Courier New") ));
		pTitle->SetFont( &messageFont );
		messageFont.DeleteObject( );
	}

	return TRUE;
}
// ErrorDlg message handlers
HBRUSH ErrorDlg::OnCtlColor( CDC *pDC, CWnd *pWnd, UINT nCtlColor )
{
	HBRUSH hBrush = CDialog::OnCtlColor( pDC, pWnd, nCtlColor );

	if( pWnd->GetDlgCtrlID() == IDC_ERR_TITLE )
	{
		pDC->SetTextColor( RGB( 128, 0, 0 ) );
	}

	return hBrush;
}

void ErrorDlg::OnBnClickedExit()
{
	PostMessage( WM_QUIT, 0, 0 );
}
